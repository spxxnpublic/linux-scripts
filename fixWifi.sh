#!/bin/bash
echo "Starting reset of wireless adapter"
echo "----------------------------------"
echo "1 of 5: Turning off wifi network adapter"
sudo ip link set link dev wlp3s0 down

echo "2 of 5: Turning down network service gui"
sudo service networkd-dispatcher stop

echo "3 of 5: Removing wifi drivers"
sudo rmmod ath10k_pci
sudo rmmod ath10k_core

echo "4 of 5: Adding wifi drivers"
sudo modprobe ath10k_core
sudo modprobe ath10k_pci

echo "5 of 5: Turning on network service gui and adapter"
sudo service networkd-dispatcher start

echo "Successfully completed the reset of the wifi adapter"
